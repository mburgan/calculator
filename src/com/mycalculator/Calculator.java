package com.mycalculator;

import java.util.Objects;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double number1 = 0;
        try {
            System.out.println("Please enter a number");
            number1 = in.nextDouble();
        }
        catch (Exception ex){
            System.out.println("You have entered invalid symbol. Only numbers are allowed");
            System.exit(0);
        }
        System.out.println("Please choose an operation: +,-,* or /");
        String operation = in.next();
        if (Objects.equals(operation, "+") || Objects.equals(operation, "-") || Objects.equals(operation, "*") || Objects.equals(operation, "/")) {
            System.out.println("Please enter a second number");
        } else {
            System.out.println("You have chosen incorrect operation type");
            System.exit(0);
        }
        double number2 = 1;
        try {
            number2 = in.nextDouble();
        }
        catch (Exception ex){
            System.out.println("You have entered invalid symbol. Only numbers are allowed");
            System.exit(0);
        }
        System.out.println("result");
        if (Objects.equals(operation, "+")) {
            System.out.println(number1 + number2);
        }
        if (Objects.equals(operation, "-")) {
            System.out.println(number1 - number2);
        }
        if (Objects.equals(operation, "*")) {
            System.out.println(number1 * number2);
        }
        if (Objects.equals(operation, "/")) {
            System.out.println(number1 / number2);
        }
    }
}
